const Auth                = require('../services/auth.js');
const {ADMIN_CREDENTIALS} = require('../config');

const query = (args, fn) => fn(args)
    .set('Authorization', `BEARER ${args.access_token}`)
    .set('Accept', 'application/json')
    .then((res) => res)
    .catch((err) => err);

/**
 * CAUTION! This is not thread-safe! If we send multiple requests at once (like in createOrder...) this will ask for 3 tokens and only 1 will be valid in the end...
 * -- ok, fixed by manipulating promises
 *
 * @param args
 * @param fn
 * @returns {Promise<*>}
 */
module.exports = async (args, fn) => {
    const AuthService = new Auth();
    // TODO several problems here:
    // - getAccessToken is not async
    // - if access token is null, sending the query is useless
    // - are we sure we have only 2 cases for status (401 / success) ?
    let tokenPromise = AuthService.getAccessTokenPromise();
    let result, token;

    if (tokenPromise) {
        token = await tokenPromise;
        if (token) {
            result = await query({access_token: token, ...args}, fn);
        }
    }

    if (!token || result && result.status === 401) {
        const access_token = await AuthService.requestTokenAsync({
            username: ADMIN_CREDENTIALS.username,
            password: ADMIN_CREDENTIALS.password,
        });

        const newArgs = {...args, access_token};
        result        = await query(newArgs, fn);
    }

    return result;
};
