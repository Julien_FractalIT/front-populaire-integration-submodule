const ensureValidToken = require('../utils/ensureValidToken');
const superagent       = require('superagent');
const asyncHelper      = require('async');

function getError(result) {
    if (!result) return null;
    let code    = result.statusCode || result.status;
    const value = result.body && result.body.value &&
                  (result.body.value.length === 1 ? result.body.value[0] : result.body.value);

    if (200 <= code && code < 300) {
        code = value && (value.codeErreur || (value.statut === 'Erreur'));
    }

    if (code) {
        return {
            code,
            message: result.body && (value.libelleErreur || value || result.body.text) || result.statusMessage || result,
        };
    }
    return null;
}

const dbgObj = (obj) => obj && Object.entries(obj).reduce((a, [k, v]) => (v === null || v === "" || v === undefined ? a : (a[k] = typeof v === "object" ? dbgObj(v) : v, a)), {});

async function get(authParams, url, queryString = "") {
    console.debug("ABOWEB get>", url, queryString, new Date());
    const result = await ensureValidToken(authParams, ({}) =>
        superagent.get(url).query(queryString),
    );
    console.debug("ABOWEB ---->", result && (result.statusCode || result.status) || result, getError(result));
    const value = result && result.body && result.body.value;
    if (value && value.length) {
        //console.debug("ABOWEB ==>", value.length + " result(s), first:", dbgObj(value[0]));
    }
    else {
        //console.debug("ABOWEB ==>", dbgObj(value || result.body));
    }
    return result;
}

async function getPaginated(authParams, url, params, limit, callback) {
    if (!limit) {
        const countResponse = await get(
            args,
            url + `/count`,
            getQueryString(_.omit(params, ["sortField", "sortOrder"]))
        );
        limit               = countResponse.body.value;
    }

    let offset          = 0;
    const limitPerBatch = 100;
    const resultList    = [];

    while (offset < limit) {
        // eslint-disable-next-line no-await-in-loop
        const {body} = await get(authParams, url, getQueryString({
            ...params,
            maxResults: Math.min(limitPerBatch, limit - offset),
            offset,
        }));

        // TODO get and return error

        if (!body || !body.value || !body.value.length) break; // no more left or error

        if (callback) {
            resultList.push(await asyncHelper.eachSeries(body.value, callback));
        }
        else {
            resultList.push(...body.value);
        }
        offset += limitPerBatch;
    }

    return resultList;
}


async function _send(method, authParams, url, data) {
    console.debug("ABOWEB " + method + ">", url, data);
    const result = await ensureValidToken(authParams, ({}) =>
        superagent[method](url).send(data)
    );
    console.debug("ABOWEB ---->", result && (result.statusCode || result.status) || result, getError(result));
    console.debug("ABOWEB ==>", result && result.body && result.body.value);
    return result;
}

async function post(authParams, url, data) {
    return await _send("post", authParams, url, data);
}

async function patch(authParams, url, data) {
    return await _send("patch", authParams, url, data);
}

async function put(authParams, url, data) {
    return await _send("put", authParams, url, data);
}

/**
 * Makes a GET query string from a parameter dict.
 * { a: 1 } => "?a=1"
 * Values are URL-encoded but not keys.
 *
 * @param parameter dict.
 */
const getQueryString = (params) =>
    Object.keys(params)
        .filter(
            (k) => typeof (params[k]) !== 'undefined'
        ).map(
        (k) => k + "=" + (
            typeof (params[k]) === "object" ?
                encodeURIComponent(JSON.stringify(params[k]))
                :
                encodeURIComponent(params[k])
        )
    ).join("&");

module.exports = {
    get,
    post,
    patch,
    put,
    getError,
    getQueryString,
    getPaginated,
};
