const dictionary = {
    email                     : 'login',
    nom                       : 'lastName',
    prenom                    : 'firstName',
    civilite                  : 'title',
    codeClient                : 'abowebUserId',
    codeIsoPays               : 'address.country',
    noCommande                : 'abowebOrderId',
    cp                        : 'address.postalCode',
    ville                     : 'address.city',
    pays                      : 'countryLabel',
    telephone                 : 'tel2',
    portable                  : 'tel',
    fraisPortTtc              : 'amountTtc',
    refPaiement               : 'abowebPaymentId',
    refCarteBancaire          : 'abowebCardId',
    codeTarif                 : 'abowebProductCode',
    refAbonnement             : 'abowebSubscriptionId',
    refAboEchu                : 'expiredAbowebSubscriptionId',
    societe                   : 'address.company',
    modePaiement              : 'paymentType',
    refTarif                  : 'externalProductId',
    tauxRemise                : 'discountAmount',
    creation                  : {type: 'Date', path: 'createTs'},
    modification              : {type: 'Date', path: 'updateTs'},
    puHt                      : 'priceHt',
    puTtc                     : 'price',
    montantHt                 : 'amountHt',
    montantTtc                : 'amountTtc',
    libParutionPns            : 'pnsLabel',
    libParutionDns            : 'dnsLabel',
    designationTarif          : 'productLabel',
    refFacture                : 'abowebInvoiceId',
    noFacture                 : 'invoiceNumber',
    dateDebutAbo              : {type: 'Date', path: 'subscriptionBeginTs'}, // Warning: duplicate key (dateDebutPeriodeAbonnement). Which one is used in which functions (that's why you need to COMMENT!)
    dateDebutPeriodeAbonnement: {type: 'Date', path: 'subscriptionBeginTs'},
    dateFinPeriodeAbonnement  : {type: 'Date', path: 'subscriptionEndTs'},
    dateOrdre                 : {type: 'Date', path: 'purchaseTs'},
    adresse1                  : 'address.line2', // ! address mapping
    adresse2                  : 'address.line1', // ! address mapping
    adresse3                  : 'address.line3',
    adresse4                  : 'address.line4', // not used
    adresse1Liv               : 'shippingAddress.line2', // ! address mapping
    adresse2Liv               : 'shippingAddress.line1', // ! address mapping
    adresse3Liv               : 'shippingAddress.line3',
    adresse4Liv               : 'shippingAddress.line4', // not used
    civiliteLiv               : 'shippingAddress.title',
    nomLiv                    : 'shippingAddress.lastName',
    prenomLiv                 : 'shippingAddress.firstName',
    societeLiv                : 'shippingAddress.company',
    cpLiv                     : 'shippingAddress.postalCode',
    villeLiv                  : 'shippingAddress.city',
    paysLiv                   : 'shippingAddress.countryLabel',
    portableLiv               : 'shippingAddress.tel',
    emailLiv                  : 'recipientEmail',
    dateCommande              : {type: 'Date', path: 'orderTs'},
    pasEmailing               : {path: 'receiveNewsletter', encode: v => !v, decode: v => !v},
    motPasseAbm               : 'abowebPassHash',
    // factures
    dateFacture               : {type: 'Date', path: 'invoiceTs'},
    codeNii                   : 'address.vatIdentificationNumber',
    // lignes factures
    quantity                  : 'quantity',
};

// Warning: reversed
const shippingAddressdictionary = {
    title                  : 'civiliteLiv',
    lastName               : 'nomLiv',
    firstName              : 'prenomLiv',
    line1                  : 'adresse2Liv', // ! address mapping
    line2                  : 'adresse1Liv', // ! address mapping
    line3                  : 'adresse3Liv',
    line4                  : 'adresse4Liv', // not used
    company                : 'societeLiv',
    postalCode             : 'cpLiv',
    city                   : 'villeLiv',
    countryLabel           : 'paysLiv',
    tel                    : 'portableLiv',
    country                : 'codeIsoPaysLiv',
    vatIdentificationNumber: 'codeNii',
    recipientEmail         : 'emailLiv',
    recipientAbowebId      : 'codeClientLiv',
};

module.exports = {
    dictionary,
    shippingAddressdictionary,
};
