/* eslint max-classes-per-file: ["error", 3] */
class BaseError extends Error {
  constructor(code, message = '', extra = {}) {
    if (!message) {
      // eslint-disable-next-line no-param-reassign
      message = code;
      // eslint-disable-next-line no-param-reassign
      code = 'Unknown';
    }
    super(message);
    this.name = 'BaseError';
    this.code = code;
    this.extra = extra;
  }
}
class InvalidCredentialsError extends BaseError {
  constructor(message) {
    super(401, 'InvalidCredentials', message || 'Invalid credentials');
    this.name = 'InvalidCredentialsError';
  }
}

class DataNotFound extends BaseError {
  constructor(message) {
    super(404, 'DataNotFound', message || 'Data is not found');
    this.name = 'DataNotFoundError';
  }
}
module.exports = {
  InvalidCredentialsError,
  DataNotFound,
};
