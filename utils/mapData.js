const _                                       = require('lodash');
const {dictionary, shippingAddressdictionary} = require('./dictionary');




const convertTimestamp      = (date) => new Date(date).getTime()/1000;
const convertDate           = (ts) => new Date(ts*1000);

const DECODE_TYPES = {
  "Date"  : convertTimestamp,
};

const ENCODE_TYPES = {
    "Date"  : convertDate,
};

const decodeObject = (current, dict, encoders, basePath = "", result) => {
    //if (!result) result = {...current}; // clone
    // TODO instead:
    if (!result) result = {}; // clone

    _.forEach(current, (value, key) => {

        let path = (basePath ? basePath + "." : "") + key;

        if (_.isArray(value)) {
            // Decode array items as new objects
            result[key] = value.map(item => _.isObject(item) ? decodeObject(item, dict, encoders, "") : item);
        }
        else if (_.isObject(value)) {
            // Decode as object properties of the same object (use path)
            decodeObject(value, dict, encoders, path, result);
        }
        else if (path in dict) {
            if (_.isObject(dict[path])) {
                if (encoders[dict[path].type]) {
                    value = encoders[dict[path].type](value);
                }
                else if (dict[path].decode) {
                    value = dict[path].decode(value);
                }

                _.set(result, dict[path].path, value);
            }
            else {
                _.set(result, dict[path], value);
            }
        }
        else { // no mapping
            if (!_.isNil(value)) { // remove null non-mapped values to reduce weight)
                result[key] = current[key];
            }
        }
    });
    return result;
};

const decodeAbowebObject = (obj) => decodeObject(obj, dictionary, DECODE_TYPES);

const encodeAbowebObject = (obj) => {
    let reverseDict = {};
    _.forEach(dictionary, (value, key) => {
        if (typeof value === 'object') {
            reverseDict[value.path] = {
                ...value,
                path: key,
                decode: value.encode,
            };
        }
        else {
            reverseDict[value] = key;
        }
    });
    return decodeObject(obj, reverseDict, ENCODE_TYPES);
};




const encodeShippingAdress = (shippingAddress) => // // Warning: shippingAddressdictionary is reversed
    decodeObject(shippingAddress, shippingAddressdictionary, ENCODE_TYPES);

const omit                  = (obj, keys) => {
    const shallowCopy = Object.assign({}, obj);
    keys.map((key) => {
        if (Object.keys(obj).includes(key)) {
            delete shallowCopy[key];
        }
    });
    return shallowCopy;
};

module.exports = {
    decodeAbowebObject,
    encodeAbowebObject,
    omit,
    encodeShippingAdress,
};
