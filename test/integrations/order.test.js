const { assert } = require('chai');
const Order = require('../../services/order');
const Auth = require('../../services/auth');
const { PAYMENT_MODE_REF_NUM } = require('../../config');

const commonClientCode = 22695;

describe('order()', () => {
  context('fetching stock', () => {
    it('should return a list of available stock', async () => {
      const orderService = new Order();
      const stockList = await orderService.getStock({
      });
      assert.typeOf(stockList, 'array');
    });
  });
  context('fetching offers', () => {
    it('should return a list of offers', async () => {
      const orderService = new Order();
      const offers = await orderService.getOffers({
      });
      assert.typeOf(offers, 'array');
      assert.containsAllKeys(offers[0], ['refTarif', 'desiTarif']);
    });

    it('should get a refTarif 6 from the code "F-FP-W4-29" ', async () => {
      const codeTarif = 'F-FP-W4-29';
      const expectedrefTarif = 6;

      const orderService = new Order();
      const { statusCode, body } = await orderService.getOfferByCode({
        codeTarif,
      });
      assert.equal(statusCode, 200);
      assert.typeOf(body, 'object');
      assert.containsAllKeys(body, ['value']);
      assert.typeOf(body.value, 'array');
      assert.lengthOf(body.value, 1);
      const foundOffer = body.value[0];
      assert.equal(foundOffer.codeTarif, codeTarif);
      assert.equal(foundOffer.refTarif, expectedrefTarif);
    });
  });
  context('creating order', () => {
    let orderCreated;
    it('should create an order and return the details', async () => {
      const orderService = new Order();

      const orderDetails = {
        abowebUserId: commonClientCode,
        nom: 'LE NOM',
        prenom: 'LE PRENOM',
        email: 'unemail@abcdTEST1234.com',
        portable: '0123456789',
        noCommandeBoutique: 'JHHU56UY',
        nePasModifierClient: 1,
        refSociete: 1,
        items: [
          {
            abowebProductCode: 'F-FP-W4-29',
            details: {
              quantity: 2,
              paymentType: 'card',
              amountTtc: 48,
              typeAdresseLiv: 0,
            },
          },
          {
            abowebProductCode: 'FDPWEB',
            details: {
              quantity: 1,
              paymentType: 'card',
              discountAmount: 10,
              amountTtc: 12,
              typeAdresseLiv: 0,
            },
          },
        ],
      };
      orderCreated = await orderService.orderItems({
        orderDetails,
      });
      assert.typeOf(orderCreated, 'object');
      assert.containsAllKeys(orderCreated, ['noCommande', 'orderTs', 'codeClient']);
    });

    it('should POST /validate the created order', async () => {
      const orderService = new Order();
      await orderService.validateOrder({
        orderNumberList: [orderCreated.noCommande],
      });
      assert.ok('order has been validated successfully');
    });

    it('createOrder() - should retrieve details about the tarif refs, create an order and return the details', async () => {
      const orderService = new Order();

      const orderDetails = {
        abowebUserId: commonClientCode,
        lastName: 'LE NOM',
        firstName: 'LE PRENOM',
        email: 'unemail@abcdTEST1234.com',
        tel: '0123456789',
        noCommandeBoutique: 'JHHU56UY',
        nePasModifierClient: 1,
        refSociete: 1,
        items: [
          {
            abowebProductCode: 'FDPWEB',
            details: {
              refTarif: 6,
              quantity: 1,
              paymentType: 'sepa',
              amountTtc: 48,
              typeAdresseLiv: 0,
            },
          },
        ],
      };
      const result = await orderService.createOrder({
        orderDetails,
      });
      assert.typeOf(result, 'object');
    });
  });

  context.skip('GET orders', () => {
    it('should return a list of orders of a client with id 1', async () => {
      const orderService = new Order();
      const orders = await orderService.getOrdersByClient({
        abowebUserId: commonClientCode,
      });
      assert.typeOf(orders, 'object');
    });
  });

  context('GET subscriptions', () => {
    it('should return the list (limit to 5 only ) of subscriptions of a client', async () => {
      const orderService = new Order();
      const subscriptionsList = await orderService.getSubscriptionsByClient({
        limit: 5,
        abowebUserId: commonClientCode,
      });
      assert.typeOf(subscriptionsList, 'array');
      assert.equal(subscriptionsList.length > 0, true);
      assert.containsAllKeys(subscriptionsList[0], ['abowebSubscriptionId', 'productLabel']);
    }).timeout(25000);
  });

  context('creating a ADL order', () => {
    it('should create a credit card', async () => {
      const orderService = new Order();
      const orderDetails = {
        abowebUserId: commonClientCode,
        lastName: 'LE NOM',
        firstName: 'LE PRENOM',
        email: 'unemail@abcdTEST1234.com',
        tel: '0123456789',
        noCommandeBoutique: 'JHHU56UY',
        nePasModifierClient: 1,
        refSociete: 1,
        items: [
          {
            abowebProductCode: 'FDPWEB',
            details: {
              refTarif: 6,
              quantity: 1,
              paymentType: 'sepa',
              amountTtc: 48,
              typeAdresseLiv: 0,
            },
          },
        ],
      };

      const orderCreated = await orderService.createADLOrder({
        orderDetails,
        stripeToken: 'cus_HQc0QJjrmyQyAO',
      });
      assert.typeOf(orderCreated, 'object');
      assert.containsAllKeys(orderCreated, ['abowebOrderId', 'orderTs', 'abowebUserId']);
    });
  });

  context('creating an Order based on the passed payementType', () => {
    it('should create a new order', async () => {
      const orderService = new Order();
      const shippingAddress  = {
        title: 'M',
        lastName: 'Test 2',
        firstName: 'Client 2',
        line1: '',
        line2: '1 rue client 2',
        postalCode: '92101',
        countryLabel: 'ville du client 2',
        codeIso: 'FR',
      };

      const orderDetails = {
        abowebUserId: commonClientCode,
        lastName: 'LE NOM',
        firstName: 'LE PRENOM',
        email: 'unemail@abcdTEST1234.com',
        tel: '0123456789',
        noCommandeBoutique: 'JHHU56UY',
        nePasModifierClient: 1,
        refSociete: 1,
        items: [
          {
            abowebProductCode: 'FDPWEB',
            details: {
              refTarif: 6,
              quantity: 1,
              paymentType: 'sepa',
              amountTtc: 48,
              typeAdresseLiv: 0,
            },
          },
        ],
      };

      const orderCreated = await orderService.createAnOrder({
        paymentType: 'sepa',
        orderDetails,
        stripeToken: 'cus_HQc0QJjrmyQyAO',
        shippingAddress,
      });
      assert.typeOf(orderCreated, 'object');
      assert.containsAllKeys(orderCreated, ['abowebOrderId', 'orderTs', 'abowebUserId']);
    });
  });
});
