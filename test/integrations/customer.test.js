const { assert } = require('chai');

const CustomerService = require('../../services/customer');
const Auth                   = require('../../services/auth');
const { decodeAbowebObject } = require('../../utils/mapData');

const maxResults = 10;
const codeClient = 22695;
const newCustomer = {
  email: 'test2@fractal-it.fr',
  typeClient: 0, // D'où vient cette valeur, que veut-elle dire??
  origineAbm: 'ABM',
  civilite: 'M',
  nom: 'Fractal',
  prenom: 'Test',
  societe: 'Company',
  adresse1: '',
  adresse2: '1 rue du client',
  adresse3: '',
  cp: '92100',
  ville: 'ville du client',
  motPasseAbm: 'password',
  codeIsoPays: 'FR',
};

describe('GET', () => {
  context('customers list', () => {
    it('should return a list of 10 customers', async () => {
      const Customer = new CustomerService();
      const { statusCode, body } = await Customer.getCustomersList({ maxResults });

      assert.equal(statusCode, 200);
      assert.equal(body.value.length, maxResults);
    });
    it('should throw 500 error', async () => {
      const Customer = new CustomerService();
      const { status } = await Customer.getCustomersList({ maxResults: '' });
      assert.equal(status, 500);
    });
  });
  context('customer Details', () => {
    it('should return a customer details', async () => {
      const Customer = new CustomerService();
      const client = await Customer.getClientAccess({ abowebUserId: codeClient });
      assert.typeOf(client, 'object');
      assert.equal(client.abowebUserId, codeClient);
    });

    it('should return null if we pass a wrong clientCode', async () => {
      const Customer = new CustomerService();
      const { body } = await Customer._getRawCustomer({ codeClient: 0 });
      assert.equal(body.value, null);
    });
  });
  context('customer invoices', () => {
    it('should return a customer details', async () => {
      const Customer = new CustomerService();
      const invoices = await Customer.getClientInvoices({ abowebUserId: codeClient, limit: maxResults });
      assert.typeOf(invoices, 'array');
      assert.equal(invoices[0].abowebUserId, codeClient)
    });
  });
});

describe('POST', () => {
  context('create a customer', () => {
    it('should return the new customer details', async () => {
      const Customer = new CustomerService();
      const data = await Customer.createCustomer({ data: newCustomer });
      assert.typeOf(data, 'object');
      const customer = await Customer.findCustomerByEmail({ email: newCustomer.email });
      assert.typeOf(customer, 'object');
      assert.equal(customer.email, newCustomer.email);
    });
  });
});

describe('PATCH', () => {
  context('update a customer field', () => {
    it('should return the updated customer details', async () => {
      const Customer = new CustomerService();
      const data = await Customer.updateCustomer({
                email: 'test2@fractal-it.fr',
        data: { codeClient: codeClient, civilite: 'MM' },
      });
      assert.equal(typeof data, 'object');
      const customer = await Customer.findCustomerByEmail({ email: newCustomer.email });
      assert.equal(typeof customer, 'object');
      assert.equal(customer.civilite, 'MM');
    });
    it('should return 500 if updated customer without passing codeClient', async () => {
      const Customer = new CustomerService();
      const { status } = await Customer.updateCustomer({
                email: 'test2@fractal-it.fr',
        data: { civilite: 'MM' },
      });
      assert.equal(status, 500);
    });
  });
});
