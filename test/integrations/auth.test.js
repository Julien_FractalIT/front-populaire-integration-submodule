const { assert } = require('chai');

const Auth = require('../../services/auth');
const CustomerService = require('../../services/customer');
const { ADMIN_CREDENTIALS } = require('../../config');

let access_token;

describe('POST /oauth/token', () => {
  context('with valid credentials', () => {
    it('should login successfully', async () => {
      const AuthService = new Auth();
      const res = await AuthService.getToken({
        username: ADMIN_CREDENTIALS.username,
        password: ADMIN_CREDENTIALS.password,
      });
      assert.typeOf(res, 'string');
    });
  });
  context('with wrong email', () => {
    it('should throw 401 error', async () => {
      const AuthService = new Auth();
      const res = await AuthService.getToken({
        username: 'wrongadmin.webservices@frontpopulaire-test.fr',
        password: ADMIN_CREDENTIALS.password,
      });
      assert.typeOf(res, 'undefined');
    });
  });
  context('with wrong password', () => {
    it('should throw 401 error', async () => {
      const AuthService = new Auth();
      const res = await AuthService.getToken({
        username: 'admin.webservices@frontpopulaire-test.fr',
        password: 'password',
      });
      assert.typeOf(res, 'undefined');
    });
  });
});

describe('Login a user ', () => {
  context('with valid credentials', () => {
    it('should login successfully', async () => {
      const AuthService = new CustomerService();
      const res = await AuthService.login({
        email: 'johndoe@email.fr',
        password: 'password',
      });
      assert.equal(res.email, 'johndoe@email.fr');
    });
  });
  context('with wrong credentials', () => {
    it('should return 401 error - wrong email', async () => {
      const CusService = new CustomerService();
      const res = await CusService.login({
        email: 'johndoe5@email.fr',
        password: 'password',
      });
      assert.equal(res.code, 401);
    });
    it('should return 401 error - wrong password', async () => {
      const CusService = new CustomerService();
      const res = await CusService.login({
        email: 'johndoe@email.fr',
        password: 'wrongpassword',
      });
      assert.equal(res.code, 401);
    });
    it('should return 401 error - missing email', async () => {
      const CusService = new CustomerService();
      const res = await CusService.login({
        password: 'password',
      });
      assert.equal(res.code, 401);
    });
    it('should return 401 error - missing password', async () => {
      const CusService = new CustomerService();
      const res = await CusService.login({
        email: 'johndoe@email.fr',
      });
      assert.equal(res.code, 401);
    });
  });
});
