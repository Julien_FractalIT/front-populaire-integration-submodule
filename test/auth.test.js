const Auth = require('../services/auth')
const assert = require('assert')

describe('login', () => {
    it('should login successfully', async () => {
        const authService = new Auth()
        const res = await authService.login({
            username: 'admin.webservices@frontpopulaire-test.fr',
            password: 'C4942EFE'
        })
        assert.equal(typeof res, 'object')
    })
})