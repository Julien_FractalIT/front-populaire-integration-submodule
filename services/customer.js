const crypto      = require('crypto');
const asyncHelper = require('async');
const moment      = require('moment');

const {WS_ENDPOINT, EDITOR_REFERENCE}                = require('../config');
const Order                                          = require('./order');
const {decodeAbowebObject, omit, encodeAbowebObject} = require('../utils/mapData');
const api                                            = require('../utils/abowebApi');
const {getQueryString}                               = require("../utils/abowebApi");

const customerUrl               = `${WS_ENDPOINT}/aboweb/editeur/${EDITOR_REFERENCE}/client`;
const omitedKeys                = ['ancienCode'];
const {InvalidCredentialsError} = require('../utils/errorHandler');

class CustomerService {

    static hashPassword(password) {
        const sha = crypto.createHash('sha1');
        sha.update(password);
        return sha.digest('base64');
    }

    /**
     * @param {Object} credentials
     * @param {String} credentials.email
     * @param {String} credentials.password
     * @returns {Object} customer (sanitized)
     */
    async login({email, password}) {
        if (!email || !password) {
            return new InvalidCredentialsError();
        }
        const customerByEmail = await this.findCustomerByEmail({email});
        if (!customerByEmail) {
            return new InvalidCredentialsError();
        }

        const encryptedPassword = CustomerService.hashPassword(password);

        if (encryptedPassword !== customerByEmail.motPasseAbm) {
            return new InvalidCredentialsError();
        }
        return decodeAbowebObject(omit(customerByEmail, omitedKeys));
    }

    /**
     * This function does not work (TODO REMOVE)
     *
     * @description find a client by his code
     * @param {Object} params
     * @param {String} params.token
     * @param {String} params.code
     * @returns {Object} customer (NOT sanitized)
     */

    /*async findCustomerByCodeClient({code}) {
        const filters     = {codeClient: {value: code, matchMode: 'equals'}};
        const {body, err} = await superagent
            .get(customerUrl)
            .query(`maxResults=1&filters=${JSON.stringify(filters)}`)
            .set('Content-Type', 'application/json')
            .then((res) => res)
            .catch((err) => err);
        if (err) console.warn(err);
        return body && body.value ? decodeAbowebObject(body.value[0]) : null;
    }*/

    /**
     * @description find a client by his email
     * @param {Object} params
     * @param {String} params.email
     * @returns {Object} customer
     */
    async findCustomerByEmail(args) {
        const filters = {email: {value: args.email, matchMode: 'equals'}};
        const {body}  = await api.get(args, customerUrl, `maxResults=1&filters=${JSON.stringify(filters)}`);
        return body.value && body.value[0] && decodeAbowebObject(body.value[0]);
    }

    /**
     * @description create a client, key = email. If client exists, ignore input and return existing (TODO change that!!)
     * @param {Object} params
     * @param {String} params.data
     * @returns {Object} customer
     */
    async createCustomer({data, ...args}) {
        const customer = await this.findCustomerByEmail({email: data.email});
        if (customer) {
            // TODO change this behaviour
            return customer;
        }
        const {body} = await api.post(args, customerUrl, data);
        return decodeAbowebObject(body.value);
    }

    /**
     * @description create a client, from sanitized data
     * @param {Object} params
     * @param {Object} params.user
     * @returns {Object} customer
     */
    async createCustomerFromUser({user, ...args}) {
        const customer = await this.findCustomerByEmail({email: user.login});
        if (customer) {
            // TODO change this behaviour
            return customer;
        }
        const {body} = await api.post(args, customerUrl, encodeAbowebObject(user));
        // TODO handle errors
        return decodeAbowebObject(body && body.value);
    }


    /**
     * @description Update a specific client (key = abowebUserId or email)
     * @param {Object} params
     * @param {Object} params.user
     * @returns {Object} customer
     */

    async updateCustomer({user, ...args}) {
        if (!user.abowebUserId) {
            if (!user.login) {
                console.error("Missing abowebUserId and email to update customer"); // ODO handle erreor
                return null;
            }

            const customer    = await this.findCustomerByEmail({email: user.login});
            user.abowebUserId = customer && customer.abowebUserId;
        }
        // TODO handle error

        if (user.abowebUserId) {
            const {body} = await api.patch(args, `${customerUrl}/${user.abowebUserId}`, encodeAbowebObject(user));
            // TODO handle errors
            return decodeAbowebObject(body && body.value);
        }

        return null;
    }

    async getCustomersList({maxResults, limit, sortField, sortOrder, ...args}, callback) {
        // TODO refactor using getPaginated

        let count = limit || maxResults; // retrocompat - inconsistency in parameters name
        if (!count) {
            const countResponse = await api.get(args, customerUrl + `/count`, getQueryString(args));
            count               = countResponse.body && countResponse.body.value;
        }
        if (!count) return [];

        let offset          = 0;
        const limitPerBatch = 50;
        const resultList    = [];

        while (offset < count) {
            // eslint-disable-next-line no-await-in-loop
            const {body}        = await api.get(args, customerUrl, getQueryString({
                ...args,
                maxResults: Math.min(limitPerBatch, count - offset),
                sortField : sortField || "c.modification",
                sortOrder : sortOrder || -1,
                offset,
                // + withDataSubscription?
            }));
            const decodedResult = body.value.map(decodeAbowebObject);

            if (!decodedResult || decodedResult.length === 0) break; // no more left?

            if (callback) {
                await asyncHelper.eachSeries(decodedResult, callback);
            }
            else {
                resultList.push(...decodedResult);
            }
            offset += limitPerBatch;
        }

        if (!callback) {
            return resultList;
        }
    }

    /**
     * @description get a client details
     * @param {Object} params
     * @param {String} params.abowebUserId
     * @returns {Object} customer (decoded)
     */
    async getCustomer({abowebUserId, ...args}) {
        const {body} = await api.get(args, `${customerUrl}/${abowebUserId}`);
        return decodeAbowebObject(body.value);
    }

    /**
     * @description get a client details
     * @param {Object} params
     * @param {String} params.abowebUserId
     * @returns {Object} RAW customer (undecoded)
     */
    async _getRawCustomer({abowebUserId, ...args}) {
        return await api.get(args, `${customerUrl}/${abowebUserId}`);
    }

    /**
     * @description get a client details  + subscriptions
     * @param {Object} params
     * @param {String} params.abowebUserId
     * @returns {Object} customer + subscriptions (decoded)
     */
    async getClientAccess({abowebUserId, rawOutput = false}) {
        const {body}        = await this._getRawCustomer({abowebUserId});
        const orderService  = new Order();
        const subscriptions = await orderService.getSubscriptionsByClient({
            abowebUserId: abowebUserId,
        });
        const client        = {...body.value, subscriptions};
        return rawOutput ? subscriptions : decodeAbowebObject(client);
    }

    /**
     * @description get a client invoices
     * @param {Object} params
     * @param {String} params.abowebUserId
     * @returns {Object} customer
     */
    async getInvoiceLines(args, refFacture) {
        const {body} = await api.get(args,
            `${WS_ENDPOINT}/aboweb/editeur/${EDITOR_REFERENCE}/invoiceline/details/${refFacture}`,
            `maxResults=100` // normally, there will never be more than 100 lines... but beware!
        );

        return body.value.map(line => {
            // because noCommande here is not really noCommande but noCommandeBoutique!
            if (line.noCommande && line.noCommande.toString().slice(0, 2) === "pa") { // it'a a PUF Purchase
                line.noCommandeBoutique = line.noCommande;
                delete line.noCommande;
            }
            return line;
        }).map(decodeAbowebObject);
    }

    /**
     * @description get a client invoices
     * @param {Object} params
     * @param {String} params.abowebUserId
     * @returns {Object} customer
     */
    async getClientInvoices(args, withLinesDetails = false) {
        let countOfInvoices = args.limit;
        if (!countOfInvoices) {
            const countResponse = await api.get(args,
                `${WS_ENDPOINT}/aboweb/editeur/${EDITOR_REFERENCE}/facture/count`,
                `codeClient=${args.abowebUserId}`
            );
            countOfInvoices     = countResponse.body.value;
        }
        if (!countOfInvoices || countOfInvoices === 0) return [];

        let offset          = 0;
        const limitPerBatch = 50;
        const invoicesList  = [];

        while (offset < countOfInvoices) {
            // eslint-disable-next-line no-await-in-loop
            const {body} = await api.get(args,
                `${WS_ENDPOINT}/aboweb/editeur/${EDITOR_REFERENCE}/facture`,
                `maxResults=${limitPerBatch}&offset=${offset}&codeClient=${args.abowebUserId}`
            );
            invoicesList.push(...body.value);
            offset += limitPerBatch;
        }

        if (withLinesDetails) {
            await asyncHelper.eachSeries(invoicesList, async invoice => {
                invoice.lines = await this.getInvoiceLines(args, invoice.refFacture);
            });
        }

        return invoicesList.map((invoice) => decodeAbowebObject(omit(invoice, omitedKeys)));
    }


    // TODO refactor put in another "service"? :
    async getAddress(args, refAdresse) {
        const {body} = await api.get(args, `${WS_ENDPOINT}/aboweb/editeur/${EDITOR_REFERENCE}/adresse/${refAdresse}`, "");
        return body.value && body.value && decodeAbowebObject(body.value);
    }
}

module.exports = CustomerService;
