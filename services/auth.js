const superagent = require('superagent');

const {PORTAL_ENDPOINT, AUTHORIZATION_KEY} = require('../config');

let access_token         = null;
let access_token_promise = null;

class Auth {

    getAccessTokenPromise() {
        console.debug(access_token_promise ? "ABOWEB> return current token request" : "ABOWEB> no stored token");
        return access_token_promise;
    }

    async requestTokenAsync({username, password}) {
        console.debug("ABOWEB> request token", PORTAL_ENDPOINT, username, "***");
        const data    = {
            username,
            password,
            grant_type: 'password',
        };
        const promise = superagent
            .post(PORTAL_ENDPOINT)
            .send(data)
            .type('form')
            .set('Authorization', AUTHORIZATION_KEY)
            .set('Content-Type', 'application/x-www-form-urlencoded')
            .then((res) => {
                console.debug("ABOWEB> got token: "+res.body.access_token);
                return res.body.access_token;
            })
            .catch((err) => err);

        access_token_promise = promise;

        return promise;
    }


    getAccessToken() {
        return access_token;
    }

    setAccessToken(token) {
        access_token = token || null;
    }


    /**
     * @param {Object} credentials
     * @param {String} credentials.username
     * @param {String} credentials.password
     * @returns {Object} access_token
     */
    async getToken({username, password}) {
        const data = {
            username,
            password,
            grant_type: 'password',
        };
        const res  = await superagent
            .post(PORTAL_ENDPOINT)
            .send(data)
            .type('form')
            .set('Authorization', AUTHORIZATION_KEY)
            .set('Content-Type', 'application/x-www-form-urlencoded')
            .then((res) => res.body)
            .catch((err) => err);
        this.setAccessToken(res.access_token);
        return res.access_token;
    }
}

module.exports = Auth;
