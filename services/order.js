/* eslint-disable camelcase */
const asyncHelper = require('async');

const {decodeAbowebObject, omit, encodeShippingAdress}                         = require('../utils/mapData');
const {PAYMENT_MODE_REF_NUM, COMPANY_REFERENCE, WS_ENDPOINT, EDITOR_REFERENCE} = require('../config');
const ensureValidToken                                                         = require('../utils/ensureValidToken');
const api                                                                      = require('../utils/abowebApi');
const {getError}                                                               = require("../utils/abowebApi");

const subscriptionUrl = `${WS_ENDPOINT}/aboweb/editeur/${EDITOR_REFERENCE}/abonnement`;

const omitedKeys = ['refSociete', 'telecopie', 'telephoneLiv', /*'emailLiv',*/ 'pointRelaisId'];

class Order {

    /**
     * @description get offers list ( tarifs ), refTarif will then be used in an order line item
     * @param {Object} params
     * @param {String} params.editorReference
     */
    async getOffers(args) {
        const {body} = await api.getPaginated(args, `${WS_ENDPOINT}/aboweb/publisher/${EDITOR_REFERENCE}/offer`);
        return body.value;
    }

    /**
     * @description get stock list
     * @param {Object} params
     * @param {String} params.editorReference
     */

    async getStock(args) {
        const {body} = await api.get(args,
            `${WS_ENDPOINT}/aboweb/editeur/${EDITOR_REFERENCE}/stock`
        );
        return body.value;
    }

    /**
     * @description get offers list ( tarifs ), refTarif will then be used in an order line item
     * @param {Object} params
     * @param {String} params.codeTarif
     */
    async getOfferByCode(args) {
        const result = await api.get(args,
            `${WS_ENDPOINT}/aboweb/publisher/${EDITOR_REFERENCE}/offer`,
            `maxResults=1&filters=${JSON.stringify({
                codeTarif: {
                    value    : args.codeTarif,
                    matchMode: 'equals',
                },
            })}`
        );
        return result;
    }

    /**
     * @description get the orders related a client
     * @param {Object} params
     * @param {String} params.abowebUserId
     * @returns {Array} a client orders
     */

    async getOrdersByClient(args) {
        if (!args.abowebUserId) throw new Error('Aboweb error: Client code is required');

        const {body, err} = await api.get(args,
            `${WS_ENDPOINT}/aboweb/publisher/${EDITOR_REFERENCE}/order`,
            `maxResults=50&filters=${JSON.stringify({
                codeClient: {
                    value    : args.abowebUserId,
                    matchMode: 'equals',
                },
            })}`
        );
        err && console.warn(err);
        return body && body.value && body.value.map(decodeAbowebObject);
    }

    /**
     * @description get the order from its ID
     * @param {Object} params
     * @param {String} params.abowebOrderId
     * @returns {Array} a client orders
     */
    async getOrder(args) {
        if (!args.abowebOrderId) throw new Error('Aboweb error: abowebOrderId is required');

        const {body} = await api.get(args, `${WS_ENDPOINT}/aboweb/publisher/${EDITOR_REFERENCE}/order/${encodeURIComponent(args.abowebOrderId)}`);

        return decodeAbowebObject(body.value);
    }

    convertPaymentTypeToCode(paymentTypeStr) {
        switch (paymentTypeStr) {
            case 'card':
                return PAYMENT_MODE_REF_NUM.CB;
            case 'sepa':
                return PAYMENT_MODE_REF_NUM.RIB;
            case '': // prepaid
                return PAYMENT_MODE_REF_NUM.PRELEVEMENT_CB;
            default:
                throw new Error(`Aboweb error: The payment type specified is not valid. Value:${paymentTypeStr}`);
        }
    }

    /**
     * @param {Object} params
     * @param {Object} params.orderDetails
     */
    async orderItems(args) {
        const {body} = await api.post(args,
            `${WS_ENDPOINT}/aboweb/publisher/${EDITOR_REFERENCE}/order`,
            args.orderDetails
        );
        return body && body.value;
    }

    /**
     * @description create an Order based on the passed paymentType
     * @param {Object} params
     * @param {String} params.stripeToken (Is the customer stripe id cus_xxxx)
     * @returns {Object} a credit card
     */
    async createCreditCard(args) {
        const {body} = await api.post(args,
            `${WS_ENDPOINT}/aboweb/editeur/${EDITOR_REFERENCE}/cartebancaire`,
            {refSociete: COMPANY_REFERENCE, token: args.stripeToken}
        );
        return body.value;
    }

    async validateOrder(args) {
        const result = await api.put(args,
            `${WS_ENDPOINT}/aboweb/publisher/${EDITOR_REFERENCE}/order/validate`,
            args.orderNumberList || []
        );
        /* result :
        [
          {
            noCommande: 1054,
            noCommandeBoutique: 'pa568',
            codeErreur: '201',
            libelleErreur: 'Champ obligatoire non renseigné : Pays livraison',
            statut: 'Erreur'
          }
        ]
         */

        const error = getError(result);

        return error ?
            {error, result: result.body && result.body.value}
            :
            {result: result.body && result.body.value};
    }

    /**
     * - typeAdresseLiv = 0 (pas d'adresses passsée dans la commande, si une adresse perma est installée sur le client, elle sera affectée par défaut en adresse de livraison)
     * - typeAdresseliv = 2 (adresse de type PARRAINAGE, une deuxième fiche contact sera créée sur la base de l'adresse envoyée dans la commande, c'est ce contact qui portera l'abonnement et le PARRAIN la facture+règlement)
     * - typeAdresseLiv = 5 (adresse spécifique pour la commande - CF script commandeAboAdresseLiv.php - . L'adresse ne sera affectée qu'a cette commande et n'impactera pas le reste des commandes du client. Elle surcharge également une éventuelle adresse perma sur le client)
     * - typeAdresseLiv = 7 (gestion des commandes Tiers/Payant - CF script commandeMultiBeneficiaire.php -)
     * Dans le cas ou typeAdresseLiv = 2 / 5 / 7, il faut nécessairement alimenter les rubriques d'adresse de la structure lignesCommande.
     *
     *
     * @param {Object} params
     * @param {{
     *    abowebUserId: String,
     *    lastName: String,
     *    firstName: String,
     *    email: String,
     *    tel: String,
     *    noCommandeBoutique: String,
     *    items: [{
     *      abowebProductCode:  String,
     *      details: {
     *        quantity:  Number,
     *        paymentType:  String,
     *        amountTtc:  Number,
     *        typeAdresseLiv:  Number,
     *        discountAmount:  Number,
     *        + pns?
     *      }
     *    }],
     * }} params.orderDetails
     * @param {{
     *    title: String,
     *    lastName: String,
     *    firstName: String,
     *    line1: String;
     *    line2: String,
     *    line3: String,
     *    postalCode: String,
     *    countryLabel: String,
     *    codeIso: String,
     * }} params.shippingAddress
     */
    async createOrder({orderDetails, shippingAddress, isGift, autoValidate}) {
        if (isGift && !shippingAddress) throw new Error("Pour faire un cadeau, vous devez saisir l'adresse postale du destinataire");

        const shipping = shippingAddress ? encodeShippingAdress(shippingAddress) : {};

        const buildedOrderDetails = {
            codeClient         : orderDetails.abowebUserId,
            nom                : orderDetails.lastName,
            prenom             : orderDetails.firstName,
            email              : orderDetails.login || orderDetails.email, // retro-compat
            portable           : orderDetails.tel,
            noCommandeBoutique : orderDetails.noCommandeBoutique,
            nePasModifierClient: 1, // so that client data are not erased
            refSociete         : COMPANY_REFERENCE,
        };

        // eslint-disable-next-line no-param-reassign
        buildedOrderDetails.lignesCommande = await asyncHelper.map(orderDetails.items, async (commandLine) => {
            if (!commandLine.abowebProductCode) throw new Error('Missing abowebProductCode here');
            const {body: foundOfferBody} = await this.getOfferByCode({
                codeTarif: commandLine.abowebProductCode,
            });
            const foundOfferItem         = foundOfferBody && foundOfferBody.value[0];
            if (!foundOfferItem) throw new Error('No Aboweb product with code ' + commandLine.abowebProductCode);

            return {
                quantite    : commandLine.details.quantity,
                modePaiement: this.convertPaymentTypeToCode(commandLine.details.paymentType),
                montantTtc  : commandLine.details.amountTtc,
                emailLiv    : orderDetails.recipientEmail,
                //codeClientLiv : orderDetails.recipientAbowebId, // crash
                typeAdresseLiv: isGift ? 2 : shippingAddress ? 5 : 0,
                ...shipping,
                tauxRemise: commandLine.details.discountAmount,
                refTarif  : foundOfferItem.refTarif,
                pns       : commandLine.details.pns,
            };
        });

        console.debug("ABOWEB order detail", buildedOrderDetails);
        const newOrder = await this.orderItems({orderDetails: buildedOrderDetails});
        if (!newOrder || !newOrder.noCommande) throw new Error('Aboweb error: Missing new order from the response');
        if (autoValidate) {
            await this.validateOrder({
                orderNumberList: [newOrder.noCommande],
            });
        }
        return decodeAbowebObject(omit(newOrder, omitedKeys));
    }

    /**
     * @description create an ADL Order
     * @param {Object} params
     * @param {Object} params.orderDetails
     * @returns {Object} a sanitized order (The order is validated)
     */

    async createADLOrder({orderDetails, stripeToken, shippingAddress, isGift, autoValidate}) {
        const {refCb}                 = await this.createCreditCard({stripeToken});
        orderDetails.refCarteBancaire = refCb;
        const newOrder                = await this.createOrder({orderDetails, shippingAddress, isGift, autoValidate});
        return decodeAbowebObject(omit(newOrder, omitedKeys));
    }

    /**
     * @description create an Order based on the passed paymentType
     * @param {Object} params
     * @param {String} params.paymentType ('sepa' | 'card' | '')
     *    * @param {{
     *    abowebUserId: String,
     *    lastName: String,
     *    firstName: String,
     *    email: String,
     *    tel: String,
     *    noCommandeBoutique: String,
     *    items: [{
     *      abowebProductCode:  String,
     *      details: {
     *        quantity:  Number,
     *        paymentType:  String,
     *        amountTtc:  Number,
     *        typeAdresseLiv:  Number,
     *        discountAmount:  Number,
     *        + pns?
     *      }
     *    }],
     * }} params.orderDetails
     * @param {{
     *    civiliteLiv: String
     *    adresse1: String,
     *    nomLiv: String,
     *    prenomLiv: String,
     *    adresse2Liv: String,
     *    cpLiv: String,
     *    villeLiv: String,
     *    codeIsoPaysLiv: String,
     * }} params.shippingAddress
     * @param {String} params.stripeToken (Is the customer stripe id cus_xxxx)
     * @param {Boolean} params.isGift Make a "gift" order using typeAdresseliv = 2 (PARRAINAGE)
     *
     * @returns {Object} a sanitized order (The order is validated)
     */
    async createAnOrder({paymentType, orderDetails, stripeToken = '', shippingAddress = {}, isGift, autoValidate}) {
        if (paymentType === 'card' || paymentType === 'sepa') {
            return await this.createADLOrder({orderDetails, stripeToken, shippingAddress, isGift});
        }
        return await this.createOrder({orderDetails, shippingAddress, isGift, autoValidate});
    }

    /**
     * @description get all the client subscriptions via /abonnement
     * @param {Object} params
     * @param {String} params.abowebUserId (codeClient)
     * @param {String} params.limit
     */
    async getSubscriptionsByClient(args) {
        let countOfSubscriptions = args.limit;
        if (!countOfSubscriptions) {
            const countResponse  = await api.get(args, `${subscriptionUrl}/count`, `codeClient=${args.abowebUserId}`);
            countOfSubscriptions = countResponse.body.value;
        }
        if (!countOfSubscriptions || countOfSubscriptions === 0) return [];

        let offset              = 0;
        const limitPerBatch     = 50;
        const subscriptionsList = [];

        while (offset < countOfSubscriptions) {
            // eslint-disable-next-line no-await-in-loop

            const {body} = await api.get(args,
                `${subscriptionUrl}`,
                `maxResults=${limitPerBatch}&offset=${offset}&codeClient=${args.abowebUserId}`
            );
            subscriptionsList.push(...body.value);
            offset += limitPerBatch;
        }
        return subscriptionsList.map((subscription) => decodeAbowebObject(omit(subscription, omitedKeys)));
    }

    /**
     * Get a subscription by ID
     *
     * @param abowebSubscriptionId
     * @param args
     * @returns {Promise<*>}
     */
    async getSubscriptionById({abowebSubscriptionId, ...args}) {
        const {body} = await api.get(args, `${subscriptionUrl}/${abowebSubscriptionId}`);
        return body && body.value && decodeAbowebObject(body.value);
    }
}

module.exports = Order;
