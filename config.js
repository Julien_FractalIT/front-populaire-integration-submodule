const AUTHORIZATION_KEY = 'Basic YWJvd2ViOg==';

const PAYMENT_MODE_REF_NUM = {
    CHEQUE        : 1,
    CB            : 2,
    RIB           : 3,
    VIREMENT      : 4,
    PAYPAL        : 5,
    PRELEVEMENT_CB: 6,
};

if (process.env.NODE_ENV === "preprod" || process.env.NODE_ENV === "production") { // PROD
    module.exports = {
        PORTAL_ENDPOINT  : 'https://portal.mahalo-app.io/oauth/token',
        WS_ENDPOINT      : 'https://api.mahalo-app.io',
        EDITOR_REFERENCE : 756,
        COMPANY_REFERENCE: 1,
        AUTHORIZATION_KEY,
        PAYMENT_MODE_REF_NUM,
        ADMIN_CREDENTIALS: {
            username: 'admin.webservices@front-populaire.fr',
            password: '81595FAA'
        },
    };
}
else
{
    module.exports = {
        PORTAL_ENDPOINT  : 'https://portal-preprod.mahalo-app.io/oauth/token',
        WS_ENDPOINT      : 'https://api-preprod.mahalo-app.io',
        EDITOR_REFERENCE : 829,
        COMPANY_REFERENCE: 1,
        AUTHORIZATION_KEY,
        PAYMENT_MODE_REF_NUM,
        ADMIN_CREDENTIALS: {
            username: 'admin.webservices@frontpopulaire-test.fr',
            password: 'C4942EFE'
        },
    };
}